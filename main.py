from random import randint

EMPTY = 0
SHIP = 1
BUFFER_ZONE = 2
SHOT = 4
INJURED = 8
DEAD = 16


FIELD_SIZE = 10
CLASSIC_SHIPS = [(4, 1), (3, 2), (2, 3), (1, 4)]
TASK_SHIPS = [(5, 1), (4, 2), (3, 3), (1, 5), (2, 4)]


def print_message(message):
    print(message)


def print_readable_matrix_with_ships_only(field):
    result_str = ''
    for row in field:
        for cell in row:
            if cell == SHIP:
                result_str += str(SHIP)
            else:
                result_str += str(EMPTY)
        result_str += '\n'
    print(result_str[:len(result_str) - 1])


def print_matrix(field):
    result_str = ''
    for row in field:
        for cell in row:
            result_str += str(cell)
        result_str += '\n'
    print(result_str[:len(result_str) - 1])


def substitute_cells(old_cell, new_cell, field):
    for row in field:
        for i in range(len(row)):
            if row[i] == old_cell:
                row[i] = new_cell


def put_ships(ships_parameters):
    field = []
    for i in range(FIELD_SIZE):
        field.append([0] * FIELD_SIZE)

    if len(ships_parameters) > 4:
        return put_ships_carefully(ships_parameters, field)
    else:
        return put_ships_casually(ships_parameters, field)


def put_ships_casually(ships_parameters, field):
    for deck_length, ships_number in ships_parameters:
        for i in range(ships_number):
            if not put_ship_casually(field, deck_length):
                raise RuntimeError("ERROR: Unable to put a ship: deck_length==>" + str(deck_length))
    return field


def put_ships_carefully(ships_parameters, field):
    for deck_length, ships_number in ships_parameters:
        for i in range(ships_number):
            if not put_ship_carefully(field, deck_length):
                clear_field(field)
                return put_ships_carefully(ships_parameters, field)

        print('After putting ships with deck_length==>' + str(deck_length) + ':')
        # print_readable_matrix_with_ships_only(field)
        print_matrix(field)
    return field


def put_ship_carefully(field, deck_length):
    ship_alternatives = prepare_put_alternatives(field, deck_length)
    if len(ship_alternatives) == 0:
        return False
    else:
        if deck_length == 5:
            chosen_ship = choose_item_randomly(ship_alternatives)
        elif deck_length == 4:
            chosen_ship = choose_middle_dangerous_alternative(ship_alternatives, deck_length, field)
        elif deck_length == 3:
            chosen_ship = choose_most_buffer_overlaying_alternative(ship_alternatives, field)
        else:
            # 1 and 2
            chosen_ship = choose_less_dangerous_alternative(ship_alternatives, field)
        put_ship_into_field(chosen_ship, field)
        return True


def put_ship_casually(field, deck_length):
    ship_alternatives = prepare_put_alternatives(field, deck_length)
    if len(ship_alternatives) == 0:
        return False
    else:
        chosen_ship = choose_item_randomly(ship_alternatives)
        put_ship_into_field(chosen_ship, field)
        return True


def prepare_put_alternatives(field, deck_length):
    alternatives = prepare_horizontal_put_alternatives(field, deck_length)
    if deck_length > 1:
        alternatives += prepare_vertical_put_alternatives(field, deck_length)
    return alternatives


# region choose
def choose_item_randomly(alternatives):
    i = randint(0, len(alternatives) - 1)
    return alternatives[i]


def choose_most_buffer_overlaying_alternative(ship_alternatives, field):
    buffer_overlay_and_ship_tuples = []

    for ship_alternative in ship_alternatives:
        buffer_overlay = count_buffer_overlay(ship_alternative, field)
        buffer_overlay_and_ship_tuples.append((buffer_overlay, ship_alternative))

    filtered_buffer_overlay_and_ship_tuples = filter_tuples_with_max_key(
        buffer_overlay_and_ship_tuples)

    random_tuple = choose_item_randomly(filtered_buffer_overlay_and_ship_tuples)
    return random_tuple[1]


def count_buffer_overlay(ship_alternative, field):
    buffer_cells = get_buffer_zone(ship_alternative, field)
    buffers_overlay = 0

    for i, j in buffer_cells:
        if (0 <= i < len(field)) and (0 <= j < len(field[0])):
            if (field[i][j] & BUFFER_ZONE) == BUFFER_ZONE:
                buffers_overlay += 1
    return buffers_overlay


def choose_less_dangerous_alternative(ship_alternatives, field):
    danger_and_ship_tuples = []

    for ship_alternative in ship_alternatives:
        danger = count_danger(ship_alternative, field)
        danger_and_ship_tuples.append((danger, ship_alternative))

    max_value = len(field) * len(field[0]) + 1
    filtered_less_dangerous_tuples = filter_tuples_with_min_key(
        danger_and_ship_tuples, max_value)

    random_tuple = choose_item_randomly(filtered_less_dangerous_tuples)
    return random_tuple[1]


def choose_middle_dangerous_alternative(ship_alternatives, deck_length, field):
    danger_and_ship_tuples = []

    for ship_alternative in ship_alternatives:
        danger = count_danger(ship_alternative, field)
        danger_and_ship_tuples.append((danger, ship_alternative))

    max_value = len(field) * len(field[0]) + 1
    filtered_middle_dangerous_tuples = filter_tuples_with_middle_key(
        danger_and_ship_tuples, max_value, count_max_buffer(deck_length) - 3 - 1)

    if len(filtered_middle_dangerous_tuples) > 0:
        random_tuple = choose_item_randomly(filtered_middle_dangerous_tuples)
    else:
        random_tuple = danger_and_ship_tuples[0]
    return random_tuple[1]


def count_max_buffer(deck_length):
    return (deck_length + 2) * 2 + 2


def count_danger(ship_alternative, field):
    buffer_cells = get_buffer_zone(ship_alternative, field)
    danger = 0

    for i, j in buffer_cells:
        if (0 <= i < len(field)) and (0 <= j < len(field[0])):
            if field[i][j] == EMPTY:
                danger += 1
    return danger


def filter_tuples_with_max_key(tuples):
    max_item_value = 0
    for cur_tuple in tuples:
        if cur_tuple[0] > max_item_value:
            max_item_value = cur_tuple[0]

    result_tuples = []
    for cur_tuple in tuples:
        if cur_tuple[0] == max_item_value:
            result_tuples.append(cur_tuple)
    return result_tuples


def filter_tuples_with_min_key(tuples, max_value):
    min_item_value = max_value
    for cur_tuple in tuples:
        if cur_tuple[0] < min_item_value:
            min_item_value = cur_tuple[0]

    result_tuples = []
    for cur_tuple in tuples:
        if cur_tuple[0] == min_item_value:
            result_tuples.append(cur_tuple)
    return result_tuples


def filter_tuples_with_middle_key(tuples, max_possible_value, max_allowed_value):
    min_value = max_possible_value
    up_border = 0
    for cur_tuple in tuples:
        if cur_tuple[0] < min_value:
            min_value = cur_tuple[0]
        if up_border < cur_tuple[0] <= max_allowed_value:
            up_border = cur_tuple[0]

    result_tuples = []
    for cur_tuple in tuples:
        if min_value + 1 <= cur_tuple[0] <= up_border:
            result_tuples.append(cur_tuple)
    return result_tuples
# endregion choose


def clear_field(field):
    for row in field:
        for i in range(len(row)):
            row[i] = EMPTY


def put_ship_into_field(ship, field):
    for i, j in ship:
        if field[i][j] != EMPTY:
            raise RuntimeError("Cannot put the ship, cell is not EMPTY.ship==>" +
                               str(ship) + " field==>\n" + str(ship))
        else:
            field[i][j] |= SHIP
    surround_with_buffer_zone(ship, field)


def surround_with_buffer_zone(ship, field):
    buffer_cells = get_buffer_zone(ship, field)
    for i, j in buffer_cells:
        if (0 <= i < len(field)) and (0 <= j < len(field[0])):
            field[i][j] |= BUFFER_ZONE


# TODO Refactor me.
def get_buffer_zone(ship, field):
    buffer_cells = []
    if ship_horizontal(ship):
        for i, j in ship:
            buffer_cells.append((i - 1, j))
            buffer_cells.append((i + 1, j))
        # To left of the ship
        first_ship_cell = ship[0]
        if first_ship_cell[1] > 0:
            buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] - 1))
            buffer_cells.append((first_ship_cell[0], first_ship_cell[1] - 1))
            buffer_cells.append((first_ship_cell[0] + 1, first_ship_cell[1] - 1))
        # To right of the ship
        last_ship_cell = ship[len(ship) - 1]
        if last_ship_cell[1] < len(field[0]) - 1:
            buffer_cells.append((last_ship_cell[0] - 1, last_ship_cell[1] + 1))
            buffer_cells.append((last_ship_cell[0], last_ship_cell[1] + 1))
            buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] + 1))
    else:
        for i, j in ship:
            buffer_cells.append((i, j - 1))
            buffer_cells.append((i, j + 1))
        # To top of the ship
        first_ship_cell = ship[0]
        if first_ship_cell[0] > 0:
            buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] - 1))
            buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1]))
            buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] + 1))
        # To bottom of the ship
        last_ship_cell = ship[len(ship) - 1]
        if last_ship_cell[0] < len(field) - 1:
            buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] - 1))
            buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1]))
            buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] + 1))
    return buffer_cells


def ship_horizontal(ship):
    if len(ship) == 1:
        return True
    else:
        first_ship_cell = ship[0]
        second_ship_cell = ship[1]
        return first_ship_cell[0] == second_ship_cell[0]


# region Horizontal alternatives
def prepare_horizontal_put_alternatives(field, deck_length):
    alternatives = []
    row_index = 0
    for row in field:
        alternatives += prepare_row_put_alternatives(row, row_index, deck_length)
        row_index += 1
    return alternatives


def prepare_row_put_alternatives(row, row_index, deck_length):
    alternatives = []

    empty_spans = find_empty_spans(row)
    for empty_span in empty_spans:
        span_start = empty_span[0]
        span_length = empty_span[1]
        if span_length < deck_length:
            continue
        else:
            for i in range(span_length - deck_length + 1):
                alternatives.append(build_horizontal_ship_cells(
                    (row_index, span_start + i), deck_length))
    return alternatives


def build_horizontal_ship_cells(start_coordinate, deck_length):
    ship_cells = []
    for i in range(start_coordinate[1], start_coordinate[1] + deck_length):
        ship_cells.append((start_coordinate[0], i))
    return ship_cells


def find_empty_spans(series):
    empty_spans = []
    empty_cells_counter = 0
    span_starting_index = 0
    cur_cell_index = 0
    for cell in series:
        if cell == EMPTY:
            empty_cells_counter += 1
        else:
            if empty_cells_counter > 0:
                empty_spans.append((span_starting_index, empty_cells_counter))
                empty_cells_counter = 0
            span_starting_index = cur_cell_index + 1
        cur_cell_index += 1
    if empty_cells_counter > 0:
        empty_spans.append((span_starting_index, empty_cells_counter))
    return empty_spans
# endregion Horizontal alternatives


# region Vertical alternatives
def prepare_vertical_put_alternatives(field, deck_length):
    alternatives = []
    row_index = 0
    for column_index in range(0, len(field[0])):
        column = get_column(column_index, field)
        alternatives += prepare_column_put_alternatives(column, column_index, deck_length)
        row_index += 1
    return alternatives


def get_column(column_index, field):
    column = []
    for i in range(len(field)):
        column.append(field[i][column_index])
    return column


def prepare_column_put_alternatives(column, column_index, deck_length):
    alternatives = []

    empty_spans = find_empty_spans(column)
    for empty_span in empty_spans:
        span_start = empty_span[0]
        span_length = empty_span[1]
        if span_length < deck_length:
            continue
        else:
            for i in range(span_length - deck_length + 1):
                alternatives.append(build_vertical_ship_cells(
                    (span_start + i, column_index), deck_length))
    return alternatives


def build_vertical_ship_cells(start_coordinate, deck_length):
    ship_cells = []
    for i in range(start_coordinate[0], start_coordinate[0] + deck_length):
        ship_cells.append((i, start_coordinate[1]))
    return ship_cells
# endregion Vertical alternatives


def show_careful_ship_putting():
    field = put_ships(TASK_SHIPS)
    print('\n')
    print_readable_matrix_with_ships_only(field)


def show_casual_ship_putting():
    field = put_ships(CLASSIC_SHIPS)
    print_readable_matrix_with_ships_only(field)


def main():
    show_casual_ship_putting()


main()
